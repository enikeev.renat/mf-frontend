import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import eslintPlugin from 'vite-plugin-eslint'

import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		vue(),
		eslintPlugin({
			cache: false,
		}),
	],
	base: '/',
	server: {
		proxy: {
			'/api':'http://madclub.ru:9001',
			'/storage':'http://madclub.ru:9001',
		},
	},
	css: {
		preprocessorOptions: {
			scss: {
				additionalData: `
					@import "@/styles/_variables.scss";
					@import "@/styles/_mixins.scss";
				`,
			},
		},
	},
	resolve: {
		alias: [
			{
				find: '@',
				replacement: resolve(__dirname, 'src'),
			},
		],
		extensions: ['.mjs', '.js', '.json', '.jsx', '.ts', '.vue'],
	},
})
