module.exports = {
	root: true,
	env: {
		es2021: true,
		browser: true,
		node: true,
	},
	extends: [
		'plugin:vue/vue3-essential',
		'@vue/typescript/recommended',
		'@vue/prettier',
		'@vue/prettier/@typescript-eslint',
	],
	parserOptions: {
		ecmaVersion: 2020,
	},
	plugins: [],
	rules: {
		'no-unused-vars': 'off',
		'array-element-newline': 'off', //['warn', 'consistent'],
		'function-call-argument-newline': 'off', //["warn", "consistent"],
		'function-paren-newline': 'off', //["warn", "consistent"],
		'@typescript-eslint/no-explicit-any': 'off',
		'vue/no-unused-vars': 'off',
		'@typescript-eslint/explicit-module-boundary-types': 'off',
		'@typescript-eslint/no-unused-vars': 'off',
		'@typescript-eslint/no-empty-function': 'warn',
		'prefer-const': 'warn',
		'prettier/prettier': 'warn',
	},
}
