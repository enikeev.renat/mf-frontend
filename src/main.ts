import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router/index'
import { createPinia } from 'pinia'
import { quasar, quasarOptions } from '@/plugins/quasar/index'
import '@/styles/global.scss'
;(async () => {
	console.info('MODE', import.meta.env.MODE)
	console.info(import.meta.env)
	if (import.meta.env.DEV) {
		const fake = await import('@/plugins/api/fake')
		fake.default()
	}
	const app = createApp(App)
	app.config.globalProperties.$env = import.meta.env
	app.use(quasar, quasarOptions)
	app.use(router)
	app.use(createPinia())
	app.mount('#app')
})()
