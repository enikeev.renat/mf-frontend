import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
	{
		path: '/',
		name: 'home',
		component: () => import('@/pages/home/ThePageHome.vue').then((m) => m.default),
	},
]

export default routes
