import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
	{
		path: '/about',
		name: 'about',
		meta: {
			title: 'Информация',
		},
		component: () => import('@/pages/about/ThePageAbout.vue').then((m) => m.default),
	},
]

export default routes
