import { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
	{
		path: '/:_(.*)*',
		name: 'error',
		component: () => import('@/pages/error/ThePageError.vue').then((m) => m.default),
	},
]

export default routes
