import { client } from '@/plugins/api'
import { defineStore } from 'pinia'
import { IDish, IFilterOptions, FilterOptionsQuery } from '@/plugins/utils/types'

class DefaultFilterOptions extends FilterOptionsQuery {
	orderField = 'id'
	page = 1
	search = ''
	size = 30
	sortDesc = false

	constructor(data?: IFilterOptions) {
		super(data)
	}
}

export class DefaultDish implements IDish {
	id = undefined
	name = ''
	description = ''
	techMaps = []
	dishTypes = []
	visual = undefined
	recipeSteps = []

	constructor(data?: IDish) {
		if (data) {
			for (const property in data) {
				if (data.hasOwnProperty(property) && this.hasOwnProperty(property)) {
					;(<any>this)[property] = (<any>data)[property]
				}
			}
		}
	}
}

export const useKitchenDishesStore = defineStore({
	id: 'kitchenDishesStore',

	state() {
		return {
			loading: false,
			items: [] as IDish[],
			item: new DefaultDish() as IDish,

			filterOptions: new DefaultFilterOptions(),

			totalSize: undefined,
		}
	},

	actions: {
		async getAllDishes() {
			this.$patch({ loading: true })
			try {
				const { items, currentPage, totalSize } = await client.dishes.getAll(this.filterOptions)
				this.$patch({
					items,
					totalSize,
				})
				this.$patch({
					filterOptions: {
						page: currentPage,
					},
				})
			} finally {
				this.$patch({ loading: false })
			}
		},

		async getDishById(id: number | string | undefined) {
			this.$patch({ item: new DefaultDish() })
			if (id !== undefined) {
				this.$patch({ loading: true })
				try {
					const item = await client.dishes.getById(id)
					this.$patch({
						item,
					})
				} finally {
					this.$patch({ loading: false })
				}
			}
		},

		async saveDishItem() {
			this.$patch({ loading: true })
			try {
				return await client.dishes.saveItem(this.item)
			} finally {
				this.$patch({ loading: false })
			}
		},

		async deleteDishItem(id: number) {
			this.$patch({ loading: true })
			try {
				const value = await client.dishes.deleteItem(id)
				this.getAllDishes()
				return value
			} finally {
				this.$patch({ loading: false })
			}
		},
	},

	getters: {
		noItemsAll(): boolean {
			return !this.loading && !this.filterOptions.search && this.items.length <= 0
		},
	},
})
