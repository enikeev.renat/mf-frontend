import { client } from '@/plugins/api'
import { defineStore } from 'pinia'
import { IDishType, IFilterOptions, FilterOptionsQuery, IVisual } from '@/plugins/utils/types'

export class DefaultFilterOptions extends FilterOptionsQuery {
	orderField = 'id'
	page = 1
	search = ''
	size = 30
	sortDesc = false

	constructor(data?: IFilterOptions) {
		super(data)
	}
}

export class DefaultDishType implements IDishType {
	id = undefined
	name = ''
	visual = undefined

	constructor(data?: IDishType) {
		if (data) {
			for (const property in data) {
				if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property]
			}
		}
	}
}

export const useKitchenDishTypesStore = defineStore({
	id: 'kitchenDishTypesStore',

	state() {
		return {
			loading: false,
			items: [] as IDishType[],
			item: new DefaultDishType() as IDishType,

			filterOptions: new DefaultFilterOptions(),

			totalSize: undefined,
		}
	},

	actions: {
		async getAllDishTypes() {
			this.$patch({ loading: true })
			try {
				const { items, currentPage, totalSize } = await client.dishtypes.getAll(this.filterOptions)
				this.$patch({
					items,
					totalSize,
				})
				this.$patch({
					filterOptions: {
						page: currentPage,
					},
				})
			} finally {
				this.$patch({ loading: false })
			}
		},

		async getDishTypeById(id: number | string | undefined) {
			this.$patch({ item: new DefaultDishType() })
			if (id !== undefined) {
				this.$patch({ loading: true })
				try {
					const item = await client.dishtypes.getById(id)
					this.$patch({
						item,
					})
				} finally {
					this.$patch({ loading: false })
				}
			}
		},

		async saveDishTypeItem() {
			this.$patch({ loading: true })
			try {
				return await client.dishtypes.saveItem(this.item)
			} finally {
				this.$patch({ loading: false })
			}
		},

		async deleteDishTypeItem(id: number) {
			this.$patch({ loading: true })
			try {
				const value = await client.dishtypes.deleteItem(id)
				this.getAllDishTypes()
				return value
			} finally {
				this.$patch({ loading: false })
			}
		},
	},

	getters: {
		noItemsAll(): boolean {
			return !this.loading && !this.filterOptions.search && this.items.length <= 0
		},
	},
})
