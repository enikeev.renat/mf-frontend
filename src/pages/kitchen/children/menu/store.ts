import { client } from '@/plugins/api'
import { defineStore } from 'pinia'
import { IMenu, IFilterOptions, FilterOptionsQuery } from '@/plugins/utils/types'

class DefaultFilterOptions extends FilterOptionsQuery {
	orderField = 'id'
	page = 1
	search = ''
	size = 30
	sortDesc = false

	constructor(data?: IFilterOptions) {
		super(data)
	}
}

export class DefaultMenu implements IMenu {
	id? = undefined
	start = ''
	end = ''
	dayMenus = []

	constructor(data?: IMenu) {
		if (data) {
			for (const property in data) {
				if (data.hasOwnProperty(property) && this.hasOwnProperty(property)) {
					;(<any>this)[property] = (<any>data)[property]
				}
			}
		}
	}
}

export const useKitchenMenuStore = defineStore({
	id: 'kitchenMenuStore',

	state() {
		return {
			loading: false,
			items: [] as IMenu[],
			item: new DefaultMenu() as IMenu,

			filterOptions: new DefaultFilterOptions(),

			totalSize: undefined,
		}
	},

	actions: {
		async getAllMenu() {
			this.$patch({ loading: true })
			try {
				const items = await client.menu.getAll(this.filterOptions)

				this.$patch({
					items,
					totalSize: items.length,
				})
				this.$patch({
					filterOptions: {
						page: 1,
					},
				})

				/*const { items, currentPage, totalSize } = await client.menu.getAll(this.filterOptions)
				this.$patch({
					items,
					totalSize,
				})
				this.$patch({
					filterOptions: {
						page: currentPage,
					},
				})*/
			} finally {
				this.$patch({ loading: false })
			}
		},

		async getMenuById(id: number | string | undefined) {
			this.$patch({ item: new DefaultMenu() })
			if (id !== undefined) {
				this.$patch({ loading: true })
				try {
					const item = await client.menu.getById(id)
					this.$patch({
						item,
					})
				} finally {
					this.$patch({ loading: false })
				}
			}
		},

		async saveMenuItem() {
			this.$patch({ loading: true })
			try {
				return await client.menu.saveItem(this.item)
			} finally {
				this.$patch({ loading: false })
			}
		},

		async deleteMenuItem(id: number) {
			this.$patch({ loading: true })
			try {
				const value = await client.menu.deleteItem(id)
				this.getAllMenu()
				return value
			} finally {
				this.$patch({ loading: false })
			}
		},
	},

	getters: {
		noItemsAll(): boolean {
			return !this.loading && !this.filterOptions.search && this.items.length <= 0
		},
	},
})
