import { client } from '@/plugins/api'
import { defineStore } from 'pinia'
import { IIngredient, IFilterOptions, FilterOptionsQuery, IVisual } from '@/plugins/utils/types'

export class DefaultFilterOptions extends FilterOptionsQuery {
	orderField = 'id'
	page = 1
	search = ''
	size = 30
	sortDesc = false

	constructor(data?: IFilterOptions) {
		super(data)
	}
}

export class DefaultIngredient implements IIngredient {
	id = undefined
	name = ''
	visual = undefined

	constructor(data?: IIngredient) {
		if (data) {
			for (const property in data) {
				if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property]
			}
		}
	}

	/*
	constructor({ visual, ...data }: IIngredientItem) {
		if (data) {
			for (const property in data) {
				if (data.hasOwnProperty(property))
					(<any>this)[property] = (<any>data)[property]
			}
		}
		if (visual)
			this.visual = typeof visual === 'object' ? visual.id : visual
	}
	* */
}

export const useKitchenIngredientsStore = defineStore({
	id: 'kitchenIngredientsStore',

	state() {
		return {
			loading: false,
			items: [] as IIngredient[],
			item: new DefaultIngredient() as IIngredient,

			filterOptions: new DefaultFilterOptions(),

			totalSize: undefined,
		}
	},

	actions: {
		async getAllIngredients() {
			this.$patch({ loading: true })
			try {
				const { items, currentPage, totalSize } = await client.ingredients.getAll(this.filterOptions)
				this.$patch({
					items,
					totalSize,
				})
				this.$patch({
					filterOptions: {
						page: currentPage,
					},
				})
			} finally {
				this.$patch({ loading: false })
			}
		},

		async getIngredientById(id: number | string | undefined) {
			this.$patch({ item: new DefaultIngredient() })
			if (id !== undefined) {
				this.$patch({ loading: true })
				try {
					const item = await client.ingredients.getById(id)
					this.$patch({
						item,
					})
				} finally {
					this.$patch({ loading: false })
				}
			}
		},

		async saveIngredientItem() {
			this.$patch({ loading: true })
			try {
				return await client.ingredients.saveItem(this.item)
			} finally {
				this.$patch({ loading: false })
			}
		},

		async deleteIngredientItem(id: number) {
			this.$patch({ loading: true })
			try {
				const value = await client.ingredients.deleteItem(id)
				this.getAllIngredients()
				return value
			} finally {
				this.$patch({ loading: false })
			}
		},
	},

	getters: {
		noItemsAll(): boolean {
			return !this.loading && !this.filterOptions.search && this.items.length <= 0
		},
	},
})
