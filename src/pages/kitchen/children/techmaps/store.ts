import { client } from '@/plugins/api'
import { defineStore } from 'pinia'
import { ITechMap, IFilterOptions, FilterOptionsQuery } from '@/plugins/utils/types'

class DefaultFilterOptions extends FilterOptionsQuery {
	orderField = 'id'
	page = 1
	search = ''
	size = 30
	sortDesc = false

	constructor(data?: IFilterOptions) {
		super(data)
	}
}

export class DefaultTechMap implements ITechMap {
	id = undefined
	name = ''
	calories = 0
	carbohydrates = 0
	description = ''
	fats = 0
	proteins = 0
	techMapItems = []

	constructor(data?: ITechMap) {
		if (data) {
			for (const property in data) {
				if (data.hasOwnProperty(property) && this.hasOwnProperty(property)) {
					;(<any>this)[property] = (<any>data)[property]
				}
			}
		}
	}
}

export const useKitchenTechMapsStore = defineStore({
	id: 'kitchenTechMapsStore',

	state() {
		return {
			loading: false,
			items: [] as ITechMap[],
			item: new DefaultTechMap() as ITechMap,

			filterOptions: new DefaultFilterOptions(),

			totalSize: undefined,
		}
	},

	actions: {
		async getAllTechMaps() {
			this.$patch({ loading: true })
			try {
				const { items, currentPage, totalSize } = await client.techmaps.getAll(this.filterOptions)
				this.$patch({
					items,
					totalSize,
				})
				this.$patch({
					filterOptions: {
						page: currentPage,
					},
				})
			} finally {
				this.$patch({ loading: false })
			}
		},

		async getTechMapById(id: number | string | undefined) {
			this.$patch({ item: new DefaultTechMap() })
			if (id !== undefined) {
				this.$patch({ loading: true })
				try {
					const item = await client.techmaps.getById(id)
					this.$patch({
						item,
					})
				} finally {
					this.$patch({ loading: false })
				}
			}
		},

		async saveTechMapItem() {
			this.$patch({ loading: true })
			try {
				return await client.techmaps.saveItem(this.item)
			} finally {
				this.$patch({ loading: false })
			}
		},

		async deleteTechMapItem(id: number) {
			this.$patch({ loading: true })
			try {
				const value = await client.techmaps.deleteItem(id)
				this.getAllTechMaps()
				return value
			} finally {
				this.$patch({ loading: false })
			}
		},
	},

	getters: {
		noItemsAll(): boolean {
			return !this.loading && !this.filterOptions.search && this.items.length <= 0
		},
	},
})
