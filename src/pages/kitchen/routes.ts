import { RouteRecordRaw } from 'vue-router'

const sidebar = () => import('@/pages/kitchen/Sidebar.vue').then((m) => m.default)
const metaMixin = {
	pageName: 'kitchen',
	hasSidebar: true,
}

const routes: RouteRecordRaw[] = [
	{
		path: '/kitchen',
		name: 'kitchen',
		meta: {
			pageName: 'kitchen',
		},
		redirect: '/kitchen/all',
	},
	{
		path: '/kitchen/all',
		name: 'kitchen-all',
		meta: {
			title: 'Кухня',
			...metaMixin,
		},
		components: {
			default: () => import('@/pages/kitchen/PageKitchen.vue').then((m) => m.default),
			sidebar,
		},
	},
	// DisheTypes
	{
		path: '/kitchen/dishtypes',
		name: 'kitchen-dishtypes',
		meta: {
			title: 'Типы блюда',
			...metaMixin,
		},
		components: {
			default: () => import('@/pages/kitchen/children/dishtypes/PageKitchenDishTypes.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/dishtypes/add',
		name: 'kitchen-dishtypes-add',
		meta: {
			title: 'Типы блюда',
			...metaMixin,
		},
		components: {
			default: () =>
				import('@/pages/kitchen/children/dishtypes/PageKitchenDishTypesItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/dishtypes/:id(\\d+)',
		name: 'kitchen-dishtypes-item',
		meta: {
			title: 'Типы блюда',
			...metaMixin,
		},
		props: true,
		components: {
			default: () =>
				import('@/pages/kitchen/children/dishtypes/PageKitchenDishTypesItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	// Menus
	{
		path: '/kitchen/menu',
		name: 'kitchen-menu',
		meta: {
			title: 'Меню',
			...metaMixin,
		},
		components: {
			default: () => import('@/pages/kitchen/children/menu/PageKitchenMenu.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/menu/add',
		name: 'kitchen-menu-add',
		meta: {
			title: 'Меню',
			...metaMixin,
		},
		components: {
			default: () => import('@/pages/kitchen/children/menu/PageKitchenMenuItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/menu/:id(\\d+)',
		name: 'kitchen-menu-item',
		meta: {
			title: 'Меню',
			...metaMixin,
		},
		props: true,
		components: {
			default: () => import('@/pages/kitchen/children/menu/PageKitchenMenuItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	// Dishes
	{
		path: '/kitchen/dishes',
		name: 'kitchen-dishes',
		meta: {
			title: 'Блюда',
			...metaMixin,
		},
		components: {
			default: () => import('@/pages/kitchen/children/dishes/PageKitchenDishes.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/dishes/add',
		name: 'kitchen-dishes-add',
		meta: {
			title: 'Блюда',
			...metaMixin,
		},
		components: {
			default: () => import('@/pages/kitchen/children/dishes/PageKitchenDishesItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/dishes/:id(\\d+)',
		name: 'kitchen-dishes-item',
		meta: {
			title: 'Блюда',
			...metaMixin,
		},
		props: true,
		components: {
			default: () => import('@/pages/kitchen/children/dishes/PageKitchenDishesItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	// TechMaps
	{
		path: '/kitchen/techmaps',
		name: 'kitchen-techmaps',
		meta: {
			title: 'Технологические карты',
			...metaMixin,
		},
		components: {
			default: () => import('@/pages/kitchen/children/techmaps/PageKitchenTechMaps.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/techmaps/add',
		name: 'kitchen-techmaps-add',
		meta: {
			title: 'Технологические карты',
			...metaMixin,
		},
		components: {
			default: () =>
				import('@/pages/kitchen/children/techmaps/PageKitchenTechMapsItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/techmaps/:id(\\d+)',
		name: 'kitchen-techmaps-item',
		meta: {
			title: 'Технологические карты',
			...metaMixin,
		},
		props: true,
		components: {
			default: () =>
				import('@/pages/kitchen/children/techmaps/PageKitchenTechMapsItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	// Ingredients
	{
		path: '/kitchen/ingredients',
		name: 'kitchen-ingredients',
		meta: {
			title: 'Ингредиенты',
			...metaMixin,
		},
		components: {
			default: () =>
				import('@/pages/kitchen/children/ingredients/PageKitchenIngredients.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/ingredients/add',
		name: 'kitchen-ingredients-add',
		meta: {
			title: 'Ингредиенты',
			...metaMixin,
		},
		components: {
			default: () =>
				import('@/pages/kitchen/children/ingredients/PageKitchenIngredientsItem.vue').then((m) => m.default),
			sidebar,
		},
	},
	{
		path: '/kitchen/ingredients/:id(\\d+)',
		name: 'kitchen-ingredients-item',
		meta: {
			title: 'Ингредиенты',
			...metaMixin,
		},
		props: true,
		components: {
			default: () =>
				import('@/pages/kitchen/children/ingredients/PageKitchenIngredientsItem.vue').then((m) => m.default),
			sidebar,
		},
	},
]

export default routes
