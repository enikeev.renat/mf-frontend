import ingredients from '@/pages/kitchen/children/ingredients/store'

const kitchen = {
	namespaced: true,

	modules: {
		ingredients,
	},
}

export default kitchen
