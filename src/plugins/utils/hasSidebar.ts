import { useRoute } from 'vue-router'

const hasSidebar = () => {
	const route = useRoute()
	return !!route.matched.find((i) => !!i.components.sidebar)
}

export default hasSidebar
