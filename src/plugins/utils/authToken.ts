import { Cookies } from 'quasar'

const authToken = (value?: string) => {
	if (value) {
		Cookies.set('auth_token', value, { path: '/', expires: 10 })
	} else {
		return Cookies.get('auth_token')
	}
}

export default authToken
