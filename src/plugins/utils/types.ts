export interface IVisual {
	filename: string
	id: number | undefined
	lowUrl: string
	mediumUrl: string
	highUrl: string
	originalUrl: string
}

export interface IDishType {
	id?: number
	name: string
	visual?: IVisual
	color?: string
}

export interface IRecipeStep {
	id?: number
	title: string
	step: number
	instruction: string
	visual?: IVisual
}

export interface IIngredient {
	id?: number
	name: string
	visual?: IVisual
}

export interface ITechMapUnit {
	id?: number
	brutto: number
	netto: number
	ingredient: IIngredient
}

export interface ITechMap {
	id?: number
	name: string
	calories: number
	carbohydrates: number
	description: string
	fats: number
	proteins: number
	techMapItems: ITechMapUnit[]
}

export interface IDish {
	id?: number
	name: string
	description: string
	techMaps?: ITechMap[]
	dishTypes?: IDishType[]
	visual?: IVisual
	recipeSteps?: IRecipeStep[]
}

export interface IUserPrivilages {
	description: string
	id: number
	name: string
}

export interface IUserRole {
	description: string
	id: number
	name: string
	privilages: IUserPrivilages[]
}

export interface IUser {
	accessToken: string
	email: string
	id: number
	roles: IUserRole[]
	tokenType: string
	username: string
}

export interface IFilterOptions {
	orderField?: string
	page?: number
	search?: string
	size?: number
	sortDesc?: boolean
}

export class FilterOptionsQuery implements IFilterOptions {
	orderField = ''
	page = 1
	search = ''
	size = 10
	sortDesc = true

	constructor(data?: IFilterOptions) {
		if (data) {
			for (const property in data) {
				if (data.hasOwnProperty(property)) (<any>this)[property] = (<any>data)[property]
			}
		}
	}
}

export interface IDayMenu {
	id?: number
	breakfast: IDish[]
	lunch: IDish[]
	dinner: IDish[]
	assigned: string
}

export interface IMenu {
	id?: number
	start: string
	end: string
	dayMenus: IDayMenu[]
}
