import moment from 'moment'
import { extendMoment } from 'moment-range'
import 'moment/locale/ru'
moment.locale('ru')
export default extendMoment(moment as any)
