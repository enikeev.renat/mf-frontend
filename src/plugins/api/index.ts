import axios from '@/plugins/api/instance'
import visuals from '@/plugins/api/modules/visuals'
import ingredients from '@/plugins/api/modules/ingredients'
import techmaps from '@/plugins/api/modules/techmaps'
import dishes from '@/plugins/api/modules/dishes'
import dishtypes from '@/plugins/api/modules/dishtypes'
import menu from '@/plugins/api/modules/menu'
import user from '@/plugins/api/modules/user'

const client = {
	visuals,
	ingredients,
	techmaps,
	dishtypes,
	dishes,
	menu,
	user,
}

export { axios, client }
