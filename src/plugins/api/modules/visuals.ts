import axios from '@/plugins/api/instance'

const visuals = {
	getAll() {
		return axios.get(`/api/visuals`).then((r) => r.data)
	},

	getById(id: number) {
		return axios.get(`/api/visuals/${id}`).then((r) => r.data)
	},

	upload(file: any, config = {}) {
		const data = new FormData()
		data.append('visual', file)
		return axios
			.post('/api/visuals', data, {
				headers: { 'Content-Type': 'multipart/form-data' },
				...config,
			})
			.then((r) => r.data)
	},
}

export default visuals
