import axios from '@/plugins/api/instance'

const user = {
	logIn(data: { password: string; username: string }) {
		return axios.post('/api/auth/signin', { ...data }).then((r) => r.data)
	},

	getCurrent() {
		return axios.get('/api/auth/current').then((r) => r.data)
	},
}

export default user
