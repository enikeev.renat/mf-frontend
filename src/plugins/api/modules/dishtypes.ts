import axios from '@/plugins/api/instance'

const ingredients = {
	getAll(data: Record<string, any>) {
		return axios.post(`/api/dishTypes`, { ...data }).then((r) => r.data)
	},

	getById(id: number | string) {
		return axios.get(`/api/dishTypes/${id}`).then((r) => r.data)
	},

	saveItem(data: Record<string, any>) {
		if (data.id) {
			return axios.patch('/api/dishTypes/', { ...data }).then((r) => r.data)
		} else {
			return axios.put('/api/dishTypes', { ...data }).then((r) => r.data)
		}
	},

	deleteItem(id: number | string) {
		return axios.delete(`/api/dishTypes/${id}`).then((r) => r.data)
	},
}

export default ingredients
