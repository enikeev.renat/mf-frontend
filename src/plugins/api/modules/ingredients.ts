import axios from '@/plugins/api/instance'

const ingredients = {
	getAll(data: Record<string, any>) {
		return axios.post(`/api/ingredients`, { ...data }).then((r) => r.data)
	},

	getById(id: number | string) {
		return axios.get(`/api/ingredients/${id}`).then((r) => r.data)
	},

	saveItem(data: Record<string, any>) {
		if (data.id) {
			return axios.patch('/api/ingredients/', { ...data }).then((r) => r.data)
		} else {
			return axios.put('/api/ingredients', { ...data }).then((r) => r.data)
		}
	},

	deleteItem(id: number | string) {
		return axios.delete(`/api/ingredients/${id}`).then((r) => r.data)
	},
}

export default ingredients
