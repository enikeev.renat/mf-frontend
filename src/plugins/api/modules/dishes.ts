import axios from '@/plugins/api/instance'

const dishes = {
	getAll(data: Record<string, any>) {
		return axios.post(`/api/dishes`, { ...data }).then((r) => r.data)
	},

	getById(id: number | string) {
		return axios.get(`/api/dishes/${id}`).then((r) => r.data)
	},

	saveItem(data: Record<string, any>) {
		if (data.id) {
			return axios.patch('/api/dishes', { ...data }).then((r) => r.data)
		} else {
			return axios.put('/api/dishes', { ...data }).then((r) => r.data)
		}
	},

	deleteItem(id: number | string) {
		return axios.delete(`/api/dishes/${id}`).then((r) => r.data)
	},
}

export default dishes
