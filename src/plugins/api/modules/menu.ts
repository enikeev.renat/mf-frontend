import axios from '@/plugins/api/instance'

const menu = {
	getAll(data: Record<string, any>) {
		return axios.get(`/api/rangeMenus`).then((r) => r.data)
		//return axios.post(`/api/rangeMenus`, { ...data }).then((r) => r.data)
	},

	getById(id: number | string) {
		return axios.get(`/api/rangeMenus/${id}`).then((r) => r.data)
	},

	saveItem(data: Record<string, any>) {
		if (data.id) {
			return axios.patch('/api/rangeMenus', { ...data }).then((r) => r.data)
		} else {
			return axios.put('/api/rangeMenus', { ...data }).then((r) => r.data)
		}
	},

	deleteItem(id: number | string) {
		return axios.delete(`/api/rangeMenus/${id}`).then((r) => r.data)
	},
}

export default menu
