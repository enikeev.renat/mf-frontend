import instance from './instance'
import MockAdapter from 'axios-mock-adapter'

import { IIngredient } from '@/plugins/utils/types'

// https://github.com/ctimmerm/axios-mock-adapter#readme
export default function () {
	const mock = new MockAdapter(instance, {
		delayResponse: 2000,
		onNoMatch: 'passthrough',
	})

	mock.onGet('~/api/auth/current').reply(() => {
		return [
			200,
			{
				created: '01.09.2021',
				roles: [
					{
						privileges: [
							{
								name: 'CREATE_USERS',
								description: 'Добавление пользователей',
								id: 1,
							},
							{
								name: 'READ_USERS',
								description: 'Чтение пользователей',
								id: 2,
							},
							{
								name: 'UPDATE_USERS',
								description: 'Редактирование пользователей',
								id: 3,
							},
							{
								name: 'DELETE_USERS',
								description: 'Удаление пользователей',
								id: 4,
							},
							{
								name: 'CREATE_INGREDIENTS',
								description: 'Добавление ингредиентов',
								id: 5,
							},
							{
								name: 'READ_INGREDIENTS',
								description: 'Чтение ингредиентов',
								id: 6,
							},
							{
								name: 'UPDATE_INGREDIENTS',
								description: 'Редактирование ингредиентов',
								id: 7,
							},
							{
								name: 'DELETE_INGREDIENTS',
								description: 'Удаление ингредиентов',
								id: 8,
							},
							{
								name: 'CREATE_VISUALS',
								description: 'Добавление изображений',
								id: 9,
							},
							{
								name: 'READ_VISUALS',
								description: 'Чтение изображений',
								id: 10,
							},
							{
								name: 'UPDATE_VISUALS',
								description: 'Редактирование изображений',
								id: 11,
							},
							{
								name: 'DELETE_VISUALS',
								description: 'Удаление изображений',
								id: 12,
							},
							{
								name: 'CREATE_TECH_MAPS',
								description: 'Добавление технологических карт',
								id: 13,
							},
							{
								name: 'READ_TECH_MAPS',
								description: 'Чтение технологических карт',
								id: 14,
							},
							{
								name: 'UPDATE_TECH_MAPS',
								description: 'Редактирование технологических карт',
								id: 15,
							},
							{
								name: 'DELETE_TECH_MAPS',
								description: 'Удаление технологических карт',
								id: 16,
							},
							{
								name: 'CREATE_TAGS',
								description: 'Добавление тегов',
								id: 17,
							},
							{
								name: 'READ_TAGS',
								description: 'Чтение тегов',
								id: 18,
							},
							{
								name: 'UPDATE_TAGS',
								description: 'Редактирование тегов',
								id: 19,
							},
							{
								name: 'DELETE_TAGS',
								description: 'Удаление тегов',
								id: 20,
							},
							{
								name: 'CREATE_DISHES',
								description: 'Добавление блюд',
								id: 21,
							},
							{
								name: 'READ_DISHES',
								description: 'Чтение блюд',
								id: 22,
							},
							{
								name: 'UPDATE_DISHES',
								description: 'Редактирование блюд',
								id: 23,
							},
							{
								name: 'DELETE_DISHES',
								description: 'Удаление блюд',
								id: 24,
							},
						],
						name: 'ROLE_ADMIN',
						description: 'Администратор',
						id: 1,
					},
				],
				id: 1,
				email: 'admin@admin.ru',
				enabled: true,
				username: 'Админ',
			},
		]
	})

	const sleep = (value: number) => new Promise((resolve) => setTimeout(resolve, value))

	mock.onPost('~/api/visuals').reply(async (config) => {
		const total = 3685315
		for (const progress of [0, 0.2, 0.4, 0.6, 0.8, 1]) {
			await sleep(500)
			if (config.onUploadProgress) {
				config.onUploadProgress({ loaded: total * progress, total })
			}
		}
		return [
			201,
			{
				filename: '862bbbf2cfcd429d90a836bde74a28ab.jpg',
				mediumUrl: '/storage/visual/medium/86/862bbbf2cfcd429d90a836bde74a28ab.jpg',
				lowUrl: '/storage/visual/low/86/862bbbf2cfcd429d90a836bde74a28ab.jpg',
				id: new Date().getTime(),
				originalUrl: '/storage/visual/original/86/862bbbf2cfcd429d90a836bde74a28ab.jpg',
				highUrl: '/storage/visual/high/86/862bbbf2cfcd429d90a836bde74a28ab.jpg',
			},
		]
	})

	mock.onPost('~/api/ingredients').reply((config) => {
		const data = JSON.parse(config.data)
		const totalSize = 500

		console.info(data)

		const request = {
			totalSize,
			totalPages: Math.ceil(totalSize / data.size),
			currentPage: data.page,
			items: [] as IIngredient[],
		}

		for (let i = 0; i < data.size; i++) {
			let id = (data.page - 1) * data.size + i + 1

			if (!data.sortDesc) {
				id = totalSize - id + 1
			}

			request.items.push({
				name: `Ингредиент ${id}`,
				id,
				visual: {
					filename: 'a25f6866fcdd4319a64f947c641523f7.jpg',
					mediumUrl: '/storage/visual/medium/a2/a25f6866fcdd4319a64f947c641523f7.jpg',
					lowUrl: '/storage/visual/low/a2/a25f6866fcdd4319a64f947c641523f7.jpg',
					id: 5,
					originalUrl: '/storage/visual/original/a2/a25f6866fcdd4319a64f947c641523f7.jpg',
					highUrl: '/storage/visual/high/a2/a25f6866fcdd4319a64f947c641523f7.jpg',
				},
			})
		}

		console.info(request)

		return [200, request]
	})

	mock.onGet('~/api/ingredients/6').reply(() => {
		return [
			400,
			{
				status: true,
			},
		]
	})
}
