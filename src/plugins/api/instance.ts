import axios, { AxiosRequestConfig } from 'axios'
import { authToken } from '@/plugins/utils'
import { useMainStore } from '@/store/main'

/**
 *	Here you can set config defaults for axios (see https://github.com/axios/axios#config-defaults)
 */

axios.defaults.headers['Authorization'] = `Bearer ${authToken()}`

//axios.defaults.httpsAgent = new HttpsProxyAgent('http://madclub.ru:9001')

axios.interceptors.request.use(
	(config) => {
		return config
	},
	(error) => {
		return Promise.reject(error)
	},
)

axios.interceptors.response.use(
	(response) => {
		return response
	},
	(error) => {
		statusParser(error.response.status, error?.response?.data)
		return Promise.reject(error)
	},
)

function statusParser(status: number, error: any) {
	if (status === 401) {
		useMainStore().logOut()
	}
}

export default axios
