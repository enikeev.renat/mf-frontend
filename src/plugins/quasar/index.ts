import './style.scss'

import { Quasar, Notify } from 'quasar'
import langRu from 'quasar/lang/ru'

export const quasar = Quasar

export const quasarOptions = {
	plugins: {
		Notify,
	},
	config: {
		notify: {
			position: 'top',
			html: true,
			timeout: 6000,
		},
	},
	lang: langRu,
}
