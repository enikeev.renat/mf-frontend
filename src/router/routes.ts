import home from '@/pages/home/routes'
import about from '@/pages/about/routes'
import kitchen from '@/pages/kitchen/routes'
import error from '@/pages/error/routes'

const routes = [...home, ...about, ...kitchen, ...error]

export default routes
