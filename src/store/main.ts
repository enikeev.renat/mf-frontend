import { defineStore } from 'pinia'
import { IUser } from '@/plugins/utils/types'
import { axios, client } from '@/plugins/api'
import { authToken } from '@/plugins/utils'

export const useMainStore = defineStore({
	id: 'mainStore',

	state() {
		return {
			authorized: false,
			user: {} as IUser,
		}
	},

	actions: {
		async logIn(username: string, password: string) {
			try {
				const { accessToken, ...user } = await client.user.logIn({
					password,
					username,
				})
				this.setToken(accessToken)
				this.$patch({
					user,
					authorized: true,
				})
			} catch (e) {
				throw new Error('User must be authenticated')
			}
		},

		async getCurrentUser() {
			try {
				const user = await client.user.getCurrent()
				this.$patch({
					user,
					authorized: true,
				})
			} catch (e) {
				throw new Error('User must be authenticated')
			}
		},

		logOut() {
			this.setToken()
			this.$patch({
				authorized: false,
			})
		},

		setToken(token = '-') {
			authToken(token)
			axios.defaults.headers['Authorization'] = `Bearer ${token}`
		},
	},
})
