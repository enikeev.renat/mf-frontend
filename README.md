# Первая установка frontend

```
npm ci
npm run prepare
```

## Запуск локально
```
npm run dev
```

### Тестовые локальные запросы

Для перехвата роута и подмены тестовыми данными пропишите инструкции в файле `\src\plugins\api\fake.ts`  


### Иконки [material](https://fonts.google.com/icons)
