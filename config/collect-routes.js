import { appendFileSync, readdirSync, rmdirSync, existsSync, mkdirSync, readFileSync } from 'fs';
import {resolve} from 'path'

const collectionDir = 'config/collections';

console.info('****************sss**************')

rmdirSync('config/collections', { recursive: true },function(err) {
	if (err) {
		console.log("err", err)
		throw err
	} else {
		console.log("Successfully removed the empty directory!")
	}
})

if (!existsSync(collectionDir)){
	mkdirSync(collectionDir);
}

const routes = readdirSync(`src/pages/`).map(dir => {
	return readdirSync(`src/pages/${dir}/`)
		.filter(name => name === 'routes.ts')
		.map(name => {
			const path = `src/pages/${dir}/${name}`
			let data = readFileSync(path, 'utf8');
			console.log(path)
			console.log(data)
			return path;
		})
}).flat()

console.info(routes);

console.info('****************end**************')

appendFileSync(`${collectionDir}/collect-routes.ts`, 'Hello content!', function (err) {
	if (err) throw err;
	console.log('Saved!');
});

